# -*- coding: utf-8 -*-

#ГОСТ Р 34.10-2001

from os import urandom
import numpy as np
from Crypto import Random
import hash
import cipher
import pbkdf2 as p2

SIZE = 32

from codecs import getdecoder
from codecs import getencoder
_hexdecoder = getdecoder("hex")
_hexencoder = getencoder("hex")

class curve():
    def __init__(self, p, q, a, b, x, y):
        self.p = bytes2long(p)
        self.q = bytes2long(q)
        self.a = bytes2long(a)
        self.b = bytes2long(b)
        self.x = bytes2long(x)
        self.y = bytes2long(y)
        r1 = self.y * self.y % self.p
        r2 = ((self.x * self.x + self.a) * self.x + self.b) % self.p
        if r2 < 0:
            r2 += self.p
        if r1 != r2:
            raise ValueError("Invalid parameters")
            
        
    def exp(self, degree, x=None, y=None):
        p = self.p
        a = self.a
        x = x or self.x
        y = y or self.y
        X, Y, Z = 1, 1, 0
        degree_bin = bin(degree)[2:]
        i = len(degree_bin) - 1
        if i == 0:
            raise ValueError("Bad degree value")
        while i >= 0:
            # Doubling
            if Z != 0:
                lm2 = X * X % p * 3
                lm1 = Z * Z * a % p
                lm1 += lm2
                lm2 = Y * Z % p
                lm3 = lm2 * X * Y % p
                lm5 = lm3 * 8
                lm4 = lm1 * lm1 % p - lm5
                lm5 = lm2 * 2 % p
                lm6 = lm5 * 2 * lm5 % p
                X = lm4 * lm5 % p
                lm7 = Y * Y % p * lm6
                Y = ((lm3 * 4 - lm4) * lm1 - lm7) % p
                Z = lm2 * lm6 % p
                if X < 0:
                    X += p
                if Y < 0:
                    Y += p
                if Z < 0:
                    Z += p
            # Adding
            if degree_bin[-i - 1] == "1":
                if Z == 0:
                    X, Y, Z = x, y, 1
                else:
                    lm1 = y * Z % p - Y
                    lm3 = x * Z % p - X
                    lm2 = lm3 * lm3 % p
                    lm4 = lm2 * lm3 % p
                    lm5 = 2 * X * lm2 % p
                    lm6 = lm1 * lm1 * Z % p - lm4 - lm5
                    lm5 = Y * lm4 % p
                    Y = ((lm2 * X - lm6) * lm1 - lm5) % p
                    X = lm3 * lm6 % p
                    Z = Z * lm4 % p
                    if X < 0:
                        X += p
                    if Y < 0:
                        Y += p
                    if Z < 0:
                        Z += p
            i -= 1
        if Z == 0:
            return -1, -1
        lm1 = modinvert(Z, p)
        r1, r2 = X * lm1 % p, Y * lm1 % p
        return r1, r2
            


def modinvert(a, n):
    if a < 0:
        # k^-1 = p - (-k)^-1 mod p
        return n - modinvert(-a, n)
    t, newt = 0, 1
    r, newr = n, a
    while newr != 0:
        quotinent = r // newr
        t, newt = newt, t - quotinent * newt
        r, newr = newr, r - quotinent * newr
    if r > 1:
        return -1
    if t < 0:
        t = t + n
    return t

def hexenc(data):
    return _hexencoder(data)[0].decode("ascii")


def bytes2long(raw):
    return int(hexenc(raw), 16)
    
def long2bytes(n, size=32):
    res = hex(int(n))[2:].rstrip("L")
    if len(res) % 2 != 0:
        res = "0" + res
    s = hexdec(res)
    if len(s) != size:
        s = (size - len(s)) * b'\x00' + s
    return s

def hexdec(data):
    return _hexdecoder(data)[0]
   



class GOST_34_10_2001():
    # Curve parameters are the following: p, q, a, b, x, y
    #GostR3410_2001
    p = bytearray("8000000000000000000000000000000000000000000000000000000000000431".decode('hex'))
    q = bytearray("8000000000000000000000000000000150FE8A1892976154C59CFC193ACCF5B3".decode('hex'))
    a = bytearray("0000000000000000000000000000000000000000000000000000000000000007".decode('hex'))
    b = bytearray("5FBFF498AA938CE739B8E022FBAFEF40563F6E6A3472FC2A514C0CE9DAE23B7E".decode('hex'))
    x = bytearray("0000000000000000000000000000000000000000000000000000000000000002".decode('hex'))
    y = bytearray("08E2A8A0E65147D4BD6316030E16D19C85C97F0A9CA267122B96ABBCEA7E8FC8".decode('hex'))
    c = curve(p, q, a, b, x, y)    
    solt_sz = 10    
    key_sz = 32 #16
    hmac_sz = 32
    password = '12345'
    
    def gen_keys(self):
        private_key = Random.new().read(self.key_sz)
        self.str_key = private_key
        private_key = [ord(symbol) for symbol in private_key]
        self.init_sign(private_key)
        return private_key

    def save(self, private_key, password): 
        solt = Random.new().read(self.solt_sz)
        key_ = p2.pbkdf2(password, solt, 1, self.key_sz)
        ciph = cipher.cipher("PKCS#5", "ECB", operation_type = "cipher", key=key_)
        private_key = ciph.update(private_key)
        self.priv_k = bytes2long(np.array(private_key))       
        
        private_key = np.array(private_key).reshape(1,len(private_key) * len(private_key[0]))[0]
        f = open('box.txt','w+')
        solt = [ord(s) for s in solt]
        data = list(private_key) + list(solt) #f.read()
        for i in private_key :
            f.write(chr(i))
        f.write(np.array(solt)) 
        
        hmac = hash.HMAC(self.str_key)
        a = hmac.update(data)  
        f.write(bytes(a))  
        f.close()
    
    def load(self):
        f = open('box.txt','rb')
        data = f.read()
        #solt = data[self.key_sz : self.key_sz + self.solt_sz]
        private_key = data[:self.key_sz]
        private_key = [ord(i) for i in private_key]
        #hmac = data[self.key_sz + self.solt_sz : self.key_sz + self.solt_sz + self.hmac_sz]        
        f.close()
        return private_key

    def init_sign(self, private_key):
        self.private_key = private_key
    
    def init_check(self, public_key = None):
        private_key = np.array(self.load())
        private_key = bytes2long(private_key)
        pubX, pubY = self.c.exp(private_key)
        return pubX, pubY
    
    def update(self, data):
        data = [ord(symbol) for symbol in data]
        private_key = self.gen_keys()
        self.save(private_key, self.password)
        
        private_key = bytes2long(np.array(private_key))
        H = hash.hash_grostl(data)
        self.hash_data = H.update()
        self.hash_data = np.array(self.hash_data)
        
        
    
    def sign(self, curve, private_key, digest, size=SIZE):
        if len(digest) != size:
            raise ValueError("Invalid digest length")
        q = curve.q
        e = bytes2long(digest) % curve.q
        if e == 0:
            e = 1
        while True:
            k = bytes2long(urandom(size)) % q
            if k == 0:
                continue
            r, _ = curve.exp(k)
            r %= q
            if r == 0:
                continue
            d = private_key * r
            k *= e
            s = (d + k) % q
            if s == 0:
                continue
            break
        return long2bytes(s) + long2bytes(r)
    
    def verify(self, curve, pubkeyX, pubkeyY, digest, signature, size=SIZE):
        if len(digest) != size:
            raise ValueError("Invalid digest length")
        if len(signature) != size * 2:
            raise ValueError("Invalid signature length")
        q = curve.q
        p = curve.p
        s = bytes2long(signature[:size])
        r = bytes2long(signature[size:])
        if r <= 0 or r >= q or s <= 0 or s >= q:
            return False
        e = bytes2long(digest) % curve.q
        if e == 0:
            e = 1
        v = modinvert(e, q)
        z1 = s * v % q
        z2 = q - r * v % q
        p1x, p1y = curve.exp(z1)
        q1x, q1y = curve.exp(degree=z2, x=pubkeyX, y=pubkeyY)
        lm = q1x - p1x
        if lm < 0:
            lm += p
        lm = modinvert(lm, p)
        z1 = q1y - p1y
        lm = lm * z1 % p
        lm = lm * lm % p
        lm = lm - p1x - q1x
        lm = lm % p
        if lm < 0:
            lm += p
        lm %= q
        # This is not constant time comparison!
        if lm == r:
            return True
        return False
    
    
    

private_key = np.array([
    0x7A, 0x92, 0x9A, 0xDE, 0x78, 0x9B, 0xB9, 0xBE,
    0x10, 0xED, 0x35, 0x9D, 0xD3, 0x9A, 0x72, 0xC1,
    0x1B, 0x60, 0x96, 0x1F, 0x49, 0x39, 0x7E, 0xEE,
    0x1D, 0x19, 0xCE, 0x98, 0x91, 0xEC, 0x3B, 0x28
])
digest = np.array([
    0x2D, 0xFB, 0xC1, 0xB3, 0x72, 0xD8, 0x9A, 0x11,
    0x88, 0xC0, 0x9C, 0x52, 0xE0, 0xEE, 0xC6, 0x1F,
    0xCE, 0x52, 0x03, 0x2A, 0xB1, 0x02, 0x2E, 0x8E,
    0x67, 0xEC, 0xE6, 0x67, 0x2B, 0x04, 0x3E, 0xE5
])
signature = np.array([
    0x41, 0xAA, 0x28, 0xD2, 0xF1, 0xAB, 0x14, 0x82,
    0x80, 0xCD, 0x9E, 0xD5, 0x6F, 0xED, 0xA4, 0x19,
    0x74, 0x05, 0x35, 0x54, 0xA4, 0x27, 0x67, 0xB8,
    0x3A, 0xD0, 0x43, 0xFD, 0x39, 0xDC, 0x04, 0x93,
    0x01, 0x45, 0x6C, 0x64, 0xBA, 0x46, 0x42, 0xA1,
    0x65, 0x3C, 0x23, 0x5A, 0x98, 0xA6, 0x02, 0x49,
    0xBC, 0xD6, 0xD3, 0xF7, 0x46, 0xB6, 0x31, 0xDF,
    0x92, 0x80, 0x14, 0xF6, 0xC5, 0xBF, 0x9C, 0x40
])


A = GOST_34_10_2001()
data = 'hi, my name is Helen'
A.update(data)
s = bytearray(A.sign(A.c, A.priv_k, A.hash_data))
print s
pubX, pubY = A.init_check()
v = A.verify(A.c, pubX, pubY, A.hash_data, s)
print v
