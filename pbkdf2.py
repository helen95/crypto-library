# -*- coding: utf-8 -*-
"""
prf — псевдослучайная функция, с выходом длины hLen
P — мастер-пароль
S — соль (salt)
c — количество итераций, положительное целое число
dkLen — желаемая длина ключа (не более (2^32 — 1) * hLen)

Выходной параметр: DK — сгенерированный ключ длины dkLen
"""
import hash

def F(P, S, c, i):
    hmac = hash.HMAC(P) 
    S_ = S + str(hex(i))
    S_ = [ord(symbol) for symbol in S_]
    res_u = hmac.update(S_)
    for j in range(c):
        if j != 0:
            u = hmac.update(res_u)
            res_u ^= u
    return list(res_u)

def pbkdf2(P, S, c, dkLen):
    
    hLen = 32
    if dkLen > (2^32 - 1) * hLen :
        return -1
    
    l = (dkLen / hLen) + 1
    r = dkLen - (l - 1) * hLen
    dk = []
    for i in range(l):
        t = F(P, S, c, i)
        if i == l - 1:
            dk += t[:r]
        else:
            dk += t
            
    return dk

# if you want to check this function
"""
a = pbkdf2('password', '�s,CN�����HL�       B�j���/DX��', 1, 89)
print a
"""
