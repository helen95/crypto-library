﻿Павлова Елена Владимировна, 319 группа

Номер задания: 12



Реализован :
1) шифр // AES-256
(полностью)

Весь алгоритм реализован в ipython notebook

Все тесты можно произввадить непосредственно в ноутбуке, примеры тестирования есть внизу


пример тестирования:


data = "asdfghjk,mnbvcxsertyuil,mnbvdertyhbhujnqwertyuiosdfghjkxcvbnmkjhgfdssdfghjkjhgfdkfkdl"

input_bytes = [ord(symbol) for symbol in data]

t = cipher("PKCS#5", "CFB", "sipher")   
# первый аргумент = ["PKCS#5", "ISO_10126", "ANSI_X.923" ] -- padding_mode
										
# второй аргумент = ["ECB", "CBC", "CNT", "CFB"] -- crypt_mode
										
# третий аргумент = ["decipher", "cipher"] -- on_type
res = t.update(input_bytes)

t.op_type = "decipher" 
# меняем тип операции на дешифрование
res2 = t.update(res)

2) хэш (вместе с HMAC) // Grostl-256
(полностью)
алгоритм реализван на python 
запустить можно через консоль (дополнительнх параметров не требуется)

примеры запуска
-для hash:
data = "The quick brown fox jumps over the lazy dog"
data = [ord(symbol) for symbol in data]
H = hash_grostl(data)
a = H.update()
np.set_printoptions(formatter={'int':hex})
print np.array(a)
H.doFinal()
print H.new_data

-для HMAC:
data = "The quick brown fox jumps over the lazy dog"
data = [ord(symbol) for symbol in data]
hmac = HMAC("true key")
a = hmac.update(data)
print a

3) ЭЦП (ГОСТ Р 34.10-2001)
реализован класс эллиптических кривых и сам алгоритм ЭЦП
также реализована функция pbkdf2 (файл pbkdf2.py)
пример тестирования есть в конце файла sign.py

4) Схема ключевого соглашения (ВКО на ключах ГОСТ З 34.10-2001)
файл VKO.py
пример тестирования корректности есть в конце фаула
