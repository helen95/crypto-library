# -*- coding: utf-8 -*-
import numpy as np
from sign import curve
from Crypto import Random
import sign
import cipher
import hash
import pbkdf2 as p2




class VKO():
    # Curve parameters are the following: p, q, a, b, x, y
    #GostR3410_2001
    p = bytearray("8000000000000000000000000000000000000000000000000000000000000431".decode('hex'))
    q = bytearray("8000000000000000000000000000000150FE8A1892976154C59CFC193ACCF5B3".decode('hex'))
    a = bytearray("0000000000000000000000000000000000000000000000000000000000000007".decode('hex'))
    b = bytearray("5FBFF498AA938CE739B8E022FBAFEF40563F6E6A3472FC2A514C0CE9DAE23B7E".decode('hex'))
    x = bytearray("0000000000000000000000000000000000000000000000000000000000000002".decode('hex'))
    y = bytearray("08E2A8A0E65147D4BD6316030E16D19C85C97F0A9CA267122B96ABBCEA7E8FC8".decode('hex'))
    c = curve(p, q, a, b, x, y)    
    solt_sz = 10    
    key_sz = 32 
    hmac_sz = 32
    password = '12345'
    
    def init(self, private_key):
        self.private_key = private_key    
         
    def gen_keys(self):
        private_key = Random.new().read(self.key_sz)
        self.str_key = private_key
        private_key = [ord(symbol) for symbol in private_key]
        self.init(private_key)
        return private_key

    def save(self, private_key, password): 
        solt = Random.new().read(self.solt_sz)
        key_ = p2.pbkdf2(password, solt, 1, self.key_sz)
        ciph = cipher.cipher("PKCS#5", "ECB", operation_type = "cipher", key=key_)
        private_key = ciph.update(private_key)
        self.priv_k = sign.bytes2long(np.array(private_key))       
        
        private_key = np.array(private_key).reshape(1,len(private_key) * len(private_key[0]))[0]
        f = open('box.txt','w+')
        solt = [ord(s) for s in solt]
        data = list(private_key) + list(solt) #f.read()
        for i in private_key :
            f.write(chr(i))
        f.write(np.array(solt)) 
        
        hmac = hash.HMAC(self.str_key)
        a = hmac.update(data)  
        f.write(bytes(a))  
        f.close()
    
    def load(self):
        f = open('box.txt','rb')
        data = f.read()
        #solt = data[self.key_sz : self.key_sz + self.solt_sz]
        private_key = data[:self.key_sz]
        private_key = [ord(i) for i in private_key]
        #hmac = data[self.key_sz + self.solt_sz : self.key_sz + self.solt_sz + self.hmac_sz]        
        f.close()
        return private_key
        
    def kek(self, curve, private_key, ukm, pubkey):
        private_key = sign.bytes2long(np.array(private_key))   
        key = curve.exp(private_key, pubkey[0], pubkey[1])
        key = curve.exp(sign.bytes2long(24 * b'\x00' + ukm), key[0], key[1])
        key = str(key[0]) + str(key[1])
        key = [ord(i) for i in key]
        H = hash.hash_grostl(key)
        a = H.update()
        return a


public_key = [0x00, 0x01, 0x00, 0x01, 0x00, 0x01, 0x00, 0x01, 0x00, 0x01, 0x00, 0x01, 0x00, 0x01,
              0x00, 0x01, 0x00, 0x01, 0x00, 0x01, 0x00, 0x01, 0x00, 0x01, 0x00, 0x01, 0x00, 0x01,
              0x00, 0x01, 0x00, 0x01]        
vko = VKO()      
vko.gen_keys()
ukm = bytearray(Random.new().read(8))
ret = vko.kek(vko.c, vko.private_key, ukm, public_key)
print ret
