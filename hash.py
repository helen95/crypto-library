import cipher # my module
import numpy as np
#import OpenSSL as ssl
#import Crypto.PublicKey
from Crypto import Random
from collections import deque


# In[444]:

class hash_grostl():
    hash_size = 32 # size of return hash
    l = 64 # size of blocks
    count_round = 10 #10
    A_row = 8
    A_col = 8
    
    def pad(self, data): 
        N = len(data)
        ret = []
        if N % self.l == 0 and N != 0:
            ret.append(data[0 : self.l])
            return ret  
        
        bit = ''
        bit = bit + '1'
        w = (-N * 8 - 65) % (self.l * 8)
        bit += '0' * w

        if len(bit) % 8 == 0:
            iter = 0
            while len(bit[iter:]) >= 8:
                data.append(int(bit[iter : iter + 8], 2))
                iter += 8
        else :
            print "SOMETHING WRONG"
        
        count_blocks = [[] for i in range(8)]
        c_b = (N * 8 + w + 65) / (self.l * 8)
        for i in range(8):
            count_blocks[-i - 1] = c_b & 255
            c_b >>= 8
        data += count_blocks
        
        offset = len(ret) * self.l
        while self.l <= len(data[offset:]) :
            ret.append(data[offset : offset + self.l])
            offset += self.l
        return ret
    
    def add_round_const(self, A, round, type):
        if type == "P":
            b = [0x00, 0x10, 0x20, 0x30, 0x40, 0x50, 0x60, 0x70] #just for groestl-256!!!
            for ind in range(len(b)):
                b[ind] = b[ind] ^ round  
            a = [0x00 for i in range(self.A_col)]
            for i in range(self.A_row):
                if i == 0:   
                    A[i] = list(np.bitwise_xor(A[i], b))
                else :
                    A[i] = list(np.bitwise_xor(A[i], a))
                    
        else : # if type == "Q"
            b = [0xff, 0xef, 0xdf, 0xcf, 0xbf, 0xaf, 0x9f, 0x8f] #just for groestl-256!!!
            for ind in range(len(b)):
                b[ind] = b[ind] ^ round
            a = [0xff for i in range(self.A_col)]
            for i in range(self.A_row):
                if i == self.A_row - 1:
                    A[i] = list(np.bitwise_xor(A[i], b))
                else :
                    A[i] = list(np.bitwise_xor(A[i], a))
        return A
    
    def sub_bytes(self, state):
        for i in range(len(state)):
            for j in range(len(state[i])):
                row = state[i][j] // 0x10
                col = state[i][j] % 0x10

                box_elem = cipher.sbox[16 * row + col]
                state[i][j] = box_elem

        return state
    
    def shift_bytes(self, state, type):
        if type == "P":
            a = [0,1,2,3,4,5,6,7]
        else : # if type == "Q"
            a = [1,3,5,7,0,2,4,6]
            
        for i in range(len(state)):
            arr = deque(state[i])
            arr.rotate(-a[i])
            state[i] = list(arr)
        return state
    
    def mix_bytes(self, state):
        for i in range(len(state)):
            s0 = self.mul_by_02(state[0][i]) ^ self.mul_by_02(state[1][i]) ^ self.mul_by_03(state[2][i]) ^ self.mul_by_04(state[3][i]) ^ self.mul_by_05(state[4][i]) ^ self.mul_by_03(state[5][i]) ^ self.mul_by_05(state[6][i]) ^ self.mul_by_07(state[7][i])
            s1 = self.mul_by_07(state[0][i]) ^ self.mul_by_02(state[1][i]) ^ self.mul_by_02(state[2][i]) ^ self.mul_by_03(state[3][i]) ^ self.mul_by_04(state[4][i]) ^ self.mul_by_05(state[5][i]) ^ self.mul_by_03(state[6][i]) ^ self.mul_by_05(state[7][i])
            s2 = self.mul_by_05(state[0][i]) ^ self.mul_by_07(state[1][i]) ^ self.mul_by_02(state[2][i]) ^ self.mul_by_02(state[3][i]) ^ self.mul_by_03(state[4][i]) ^ self.mul_by_04(state[5][i]) ^ self.mul_by_05(state[6][i]) ^ self.mul_by_03(state[7][i])
            s3 = self.mul_by_03(state[0][i]) ^ self.mul_by_05(state[1][i]) ^ self.mul_by_07(state[2][i]) ^ self.mul_by_02(state[3][i]) ^ self.mul_by_02(state[4][i]) ^ self.mul_by_03(state[5][i]) ^ self.mul_by_04(state[6][i]) ^ self.mul_by_05(state[7][i])
            s4 = self.mul_by_05(state[0][i]) ^ self.mul_by_03(state[1][i]) ^ self.mul_by_05(state[2][i]) ^ self.mul_by_07(state[3][i]) ^ self.mul_by_02(state[4][i]) ^ self.mul_by_02(state[5][i]) ^ self.mul_by_03(state[6][i]) ^ self.mul_by_04(state[7][i])
            s5 = self.mul_by_04(state[0][i]) ^ self.mul_by_05(state[1][i]) ^ self.mul_by_03(state[2][i]) ^ self.mul_by_05(state[3][i]) ^ self.mul_by_07(state[4][i]) ^ self.mul_by_02(state[5][i]) ^ self.mul_by_02(state[6][i]) ^ self.mul_by_03(state[7][i])
            s6 = self.mul_by_03(state[0][i]) ^ self.mul_by_04(state[1][i]) ^ self.mul_by_05(state[2][i]) ^ self.mul_by_03(state[3][i]) ^ self.mul_by_05(state[4][i]) ^ self.mul_by_07(state[5][i]) ^ self.mul_by_02(state[6][i]) ^ self.mul_by_02(state[7][i])
            s7 = self.mul_by_02(state[0][i]) ^ self.mul_by_03(state[1][i]) ^ self.mul_by_04(state[2][i]) ^ self.mul_by_05(state[3][i]) ^ self.mul_by_03(state[4][i]) ^ self.mul_by_05(state[5][i]) ^ self.mul_by_07(state[6][i]) ^ self.mul_by_02(state[7][i])
                
            state[0][i] = s0
            state[1][i] = s1
            state[2][i] = s2
            state[3][i] = s3
            state[4][i] = s4
            state[5][i] = s5
            state[6][i] = s6
            state[7][i] = s7

        return state
    
    def mul_by_02(self, num):
        if num < 0x80:
            res = (num << 1)
        else:
            res = (num << 1) ^ 0x1b
        return res % 0x100

    def mul_by_03(self, num):
        return (self.mul_by_02(num) ^ num)

    def mul_by_04(self, num):
        return self.mul_by_02(self.mul_by_02(num))

    def mul_by_05(self, num):
        return self.mul_by_02(self.mul_by_02(num)) ^ num

    def mul_by_07(self, num):
        return self.mul_by_02(self.mul_by_02(num)) ^ self.mul_by_02(num) ^ num
    
    def P(self, state):
        new_state = np.copy(state)
        for i in range(self.count_round):
            new_state = self.add_round_const(new_state, i, "P")
            new_state = self.sub_bytes(new_state)
            new_state = self.shift_bytes(new_state, "P")
            new_state = self.mix_bytes(new_state)
        return new_state
    
    def Q(self, state):
        new_state = np.copy(state)
        for i in range(self.count_round):
            new_state = self.add_round_const(new_state, i, "Q")
            new_state = self.sub_bytes(new_state)
            new_state = self.shift_bytes(new_state, "Q")
            new_state = self.mix_bytes(new_state)
        return new_state
    
    def add(self, a, b):
        ret = []
        if len(a) != len(b):
            return -1 #ERROR; different lens in vectors
        for i in range(len(a)):
            ret.append(list(np.bitwise_xor(a[i], b[i])))
        return ret
    
    def f(self, m, h):
        return self.add(self.add(self.P(self.add(h, m)), self.Q(np.copy(m))), h)
    
    def Omega(self, x):
        v = self.add(x, self.P(np.copy(x)))
        v = (np.array(v).T).reshape(1,64)[0]  
        return v[-self.hash_size:]
    
    def __init__(self, data):
        self.iv = [0x00 for i in range(self.l)]
        self.iv[-2] = 0x01
        new_data = self.pad(list(np.copy(data)))
        self.new_data = []
        for i in range(len(new_data)):
            state = [[] for j in range(self.A_col)]
            for r in range(self.A_row):
                for c in range(self.A_col): 
                    state[r].append(new_data[i][r + self.A_col * c])
            self.new_data.append(state)
    
    def update(self):
        iv = self.iv
        h = [[] for j in range(self.A_col)]
        for r in range(self.A_row):
            for c in range(self.A_col): 
                h[r].append(iv[r + self.A_col * c]) #(iv[r + 8 * c])
        
        for i in self.new_data:
            h = self.f(np.copy(i), h)
        
        return self.Omega(h)
    
    def doFinal(self):
        self.iv = []
        self.new_data = []
    


"""
data = "The quick brown fox jumps over the lazy dog"
data = [ord(symbol) for symbol in data]
H = hash_grostl(data)
a = H.update()
np.set_printoptions(formatter={'int':hex})
print np.array(a)
H.doFinal()
print H.new_data
"""

#Samples
# Grostl-256("")
# 0x 1a52d11d550039be16107f9c58db9ebcc417f16f736adb2502567119f0083467
# 
# Grostl-256("The quick brown fox jumps over the lazy dog")
# 0x 8c7ad62eb26a21297bc39c2d7293b4bd4d3399fa8afab29e970471739e28b301
# 
# Grostl-256("The quick brown fox jumps over the lazy dog.")
# 0x f48290b1bcacee406a0429b993adb8fb3d065f4b09cbcdb464a631d4a0080aaf

# # HMAC

# In[5]:

class HMAC:
    block_size = 32
    ipad = [0x36 for i in range(block_size)]
    opad = [0x5c for i in range(block_size)]
    key = []
    
    def gen_key(self):
        key = Random.new().read(self.block_size)
        key = [ord(symbol) for symbol in key]
        return key 

    def __init__(self, key):
        #self.key = self.gen_key()
        if len(key) == self.block_size:
            self.key = [ord(symbol) for symbol in key]
        else :
            if len(key) > self.block_size:
                a = hash_grostl(key)
                self.key = a.update() + [0x00 for i in range(self.block_size - len(a))]
            else:
                self.key = [ord(symbol) for symbol in key] + [0x00 for i in range(self.block_size - len(key))]
        
        
    def update(self, data):
        a = np.bitwise_xor(self.key, self.ipad)
        a = list(a) + data
        H = hash_grostl(a)
        hash = H.update()
        a_new = list(np.bitwise_xor(self.key, self.opad)) + list(hash)
        new_H = hash_grostl(a_new)
        new_hash = new_H.update()
        return new_hash
    
    def doFinal(self, data):
        self.key = []

"""
data = "The quick brown fox jumps over the lazy dog"
data = [ord(symbol) for symbol in data]
hmac = HMAC("true key")
a = hmac.update(data)
print a
print '-'*20
"""